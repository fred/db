---
--- domain states
---
INSERT INTO enum_object_states (id, name, types, manual, external, importance)
VALUES (33, 'premiumDomain', '{3}', 't', 't', NULL);

INSERT INTO enum_object_states_desc (state_id, lang, description)
VALUES (33, 'CS', 'VIP doména'),
       (33, 'EN', 'VIP domain');
