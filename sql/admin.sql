CREATE TABLE domain_blacklist
(
  id serial NOT NULL, -- primary key
  fqdn varchar(255) NOT NULL CONSTRAINT domain_blacklist_fqdn_lower_check CHECK(is_regex OR LOWER(fqdn) = fqdn), -- domain name or regular expression which is blocked
  valid_from timestamp NOT NULL default CURRENT_TIMESTAMP, -- from when bloc is valid
  valid_to timestamp, -- till when bloc is valid, if it is NULL, it isn't restricted
  reason varchar(255) NOT NULL, -- reason why is domain blocked
  is_regex boolean NOT NULL,
  requested_by text,
  uuid uuid NOT NULL UNIQUE default gen_random_uuid(),
  CONSTRAINT domain_blacklist_pkey PRIMARY KEY (id)
);

CREATE INDEX domain_blacklist_fqdn_idx ON domain_blacklist (fqdn) WHERE NOT is_regex;

comment on column domain_blacklist.fqdn is 'domain name or regular expression which is blocked';
comment on column domain_blacklist.valid_from is 'from when is block valid';
comment on column domain_blacklist.valid_to is 'till when is block valid, if it is NULL, it is not restricted';
comment on column domain_blacklist.reason is 'reason why is domain blocked';
comment on column domain_blacklist.is_regex is 'set to false if value in fqdn is not regex but literal';
comment on column domain_blacklist.requested_by is 'identificator of the service which set the record';
comment on column domain_blacklist.uuid IS 'uuid for external reference';
