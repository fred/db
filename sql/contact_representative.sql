CREATE TABLE IF NOT EXISTS contact_representative_history (
    history_id BIGSERIAL CONSTRAINT contact_representative_history_pkey PRIMARY KEY,
    history_uuid UUID NOT NULL UNIQUE DEFAULT gen_random_uuid(),
    id BIGINT,
    uuid UUID NOT NULL,
    contact_id BIGINT NOT NULL,
    name varchar(1024),
    organization varchar(1024),
    street1 varchar(1024) NOT NULL,
    street2 varchar(1024),
    street3 varchar(1024),
    city varchar(1024) NOT NULL,
    state_or_province varchar(1024),
    postal_code varchar(32) NOT NULL,
    country_code char(2) CONSTRAINT contact_representative_history_country_code_fkey REFERENCES enum_country, -- ISO 3166-1 alpha-2 code
    telephone varchar(64),
    email varchar(1024) NOT NULL,
    log_entry_id varchar(1024),
    log_entry_delete_id varchar(1024),
    valid_from TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    valid_to TIMESTAMP DEFAULT NULL CONSTRAINT contact_representative_history_valid_to_not_before_valid_from CHECK (valid_from <= valid_to),
    ohid_from INTEGER CONSTRAINT contact_representative_history_ohid_from_fkey REFERENCES object_history (historyid),
    ohid_to INTEGER CONSTRAINT contact_representative_history_ohid_to_fkey REFERENCES object_history (historyid),
    next_history_id BIGINT CONSTRAINT contact_representative_history_next_history_id_fkey REFERENCES contact_representative_history (history_id),
    CONSTRAINT contact_representative_history_log_entry_delete_id_valid CHECK (NOT (valid_to IS NULL AND log_entry_delete_id IS NOT NULL))
);

CREATE INDEX contact_representative_history_contact_id_valid_idx ON contact_representative_history (contact_id) WHERE valid_to IS NULL;
CREATE UNIQUE INDEX contact_representative_history_next_history_id_idx ON contact_representative_history (next_history_id);

COMMENT ON TABLE contact_representative_history IS 'Historic data from contact_representative table.
Current data will be copied here from original table in case of any change in contact_representative table';
COMMENT ON COLUMN contact_representative_history.history_id IS 'unique automatically generated identifier';
COMMENT ON COLUMN contact_representative_history.id IS 'id of the contact representative';
COMMENT ON COLUMN contact_representative_history.uuid IS 'uuid of contact representative history for external reference';
COMMENT ON COLUMN contact_representative_history.contact_id IS 'link to represented contact';
COMMENT ON COLUMN contact_representative_history.name IS 'name of contact representative';
COMMENT ON COLUMN contact_representative_history.organization IS 'full trade name of organization';
COMMENT ON COLUMN contact_representative_history.street1 IS 'part of address';
COMMENT ON COLUMN contact_representative_history.street2 IS 'part of address';
COMMENT ON COLUMN contact_representative_history.street3 IS 'part of address';
COMMENT ON COLUMN contact_representative_history.city IS 'part of address - city';
COMMENT ON COLUMN contact_representative_history.state_or_province IS 'part of address - region';
COMMENT ON COLUMN contact_representative_history.postal_code IS 'part of address - postal code';
COMMENT ON COLUMN contact_representative_history.country_code IS 'two character country code (e.g. cz) from enum_country table';
COMMENT ON COLUMN contact_representative_history.telephone IS 'telephone number';
COMMENT ON COLUMN contact_representative_history.email IS 'email address';
COMMENT ON COLUMN contact_representative_history.log_entry_id IS 'logger entry identifier';
COMMENT ON COLUMN contact_representative_history.log_entry_delete_id IS 'logger entry identifier of delete operation';
COMMENT ON COLUMN contact_representative_history.ohid_from IS 'link to object_history of represented contact from when the contact representative data was valid';
COMMENT ON COLUMN contact_representative_history.ohid_to IS 'link to object_history of represented contact until when the contact representative data was valid';
COMMENT ON COLUMN contact_representative_history.next_history_id IS 'link to next contact_representative_history record';

CREATE TABLE IF NOT EXISTS contact_representative (
    id BIGSERIAL CONSTRAINT contact_representative_pkey PRIMARY KEY,
    history_id BIGINT CONSTRAINT contact_representative_history_id_fkey REFERENCES contact_representative_history (history_id),
    uuid UUID NOT NULL UNIQUE DEFAULT gen_random_uuid(),
    contact_id BIGINT NOT NULL UNIQUE,
    name varchar(1024),
    organization varchar(1024),
    street1 varchar(1024) NOT NULL,
    street2 varchar(1024),
    street3 varchar(1024),
    city varchar(1024) NOT NULL,
    state_or_province varchar(1024),
    postal_code varchar(32) NOT NULL,
    country_code char(2) CONSTRAINT contact_representative_country_code_fkey NOT NULL REFERENCES enum_country, -- ISO 3166-1 alpha-2 code
    telephone varchar(64),
    email varchar(1024) NOT NULL,
    log_entry_id varchar(1024),
    log_entry_delete_id varchar(1024),
    valid_from TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    CONSTRAINT contact_representative_contact_id_fkey FOREIGN KEY (contact_id) REFERENCES object_registry (id),
    CONSTRAINT contact_representative_name_or_organization_not_null CHECK (NOT (name IS NULL AND organization IS NULL))
);

COMMENT ON TABLE contact_representative IS 'EU representative of Non-EU contact';
COMMENT ON COLUMN contact_representative.id IS 'unique automatically generated identifier';
COMMENT ON COLUMN contact_representative.uuid IS 'uuid of contact representative for external reference';
COMMENT ON COLUMN contact_representative.contact_id IS 'link to represented contact';
COMMENT ON COLUMN contact_representative.name IS 'name of contact representative';
COMMENT ON COLUMN contact_representative.organization IS 'full trade name of organization';
COMMENT ON COLUMN contact_representative.street1 IS 'part of address';
COMMENT ON COLUMN contact_representative.street2 IS 'part of address';
COMMENT ON COLUMN contact_representative.street3 IS 'part of address';
COMMENT ON COLUMN contact_representative.city IS 'part of address - city';
COMMENT ON COLUMN contact_representative.state_or_province IS 'part of address - region';
COMMENT ON COLUMN contact_representative.postal_code IS 'part of address - postal code';
COMMENT ON COLUMN contact_representative.country_code IS 'two character country code (e.g. cz) from enum_country table';
COMMENT ON COLUMN contact_representative.telephone IS 'telephone number';
COMMENT ON COLUMN contact_representative.email IS 'email address';
COMMENT ON COLUMN contact_representative.log_entry_id IS 'logger entry identifier';
COMMENT ON COLUMN contact_representative.log_entry_delete_id IS 'logger entry identifier of delete operation';

CREATE OR REPLACE FUNCTION contact_representative_update()
RETURNS TRIGGER
AS
$$
DECLARE
     new_ohid_from BIGINT;
     new_history_id BIGINT := NULL;
BEGIN
    SELECT historyid INTO new_ohid_from FROM object_registry WHERE id = NEW.contact_id AND type = get_object_type_id('contact'::TEXT) AND erdate IS NULL;

    IF TG_OP = 'UPDATE' OR TG_OP = 'INSERT' THEN
        IF TG_OP = 'UPDATE' AND NEW.log_entry_delete_id IS NOT NULL THEN
            IF NEW.id <> OLD.id OR
                NEW.history_id <> OLD.history_id OR
                NEW.uuid <> OLD.uuid OR
                NEW.contact_id <> OLD.contact_id OR
                NEW.name <> OLD.name OR
                NEW.organization <> OLD.organization OR
                NEW.street1 <> OLD.street1 OR
                NEW.street2 <> OLD.street2 OR
                NEW.street3 <> OLD.street3 OR
                NEW.city <> OLD.city OR
                NEW.state_or_province <> OLD.state_or_province OR
                NEW.postal_code <> OLD.postal_code OR
                NEW.country_code <> OLD.country_code OR
                NEW.telephone <> OLD.telephone OR
                NEW.email <> OLD.email OR
                NEW.log_entry_id <> OLD.log_entry_id OR
                NEW.valid_from <> OLD.valid_from
            THEN
                RAISE 'UPDATE with log_entry_delete_id NOT NULL must not change other columns, the record will be deleted and changes would be lost';
            END IF;
            DELETE FROM contact_representative
             WHERE id = NEW.id;
        ELSE
            INSERT INTO contact_representative_history (
                id,
                uuid,
                contact_id,
                name,
                organization,
                street1,
                street2,
                street3,
                city,
                state_or_province,
                postal_code,
                country_code,
                telephone,
                email,
                log_entry_id,
                log_entry_delete_id,
                valid_from,
                valid_to,
                ohid_from,
                ohid_to,
                next_history_id)
            VALUES (
                NEW.id,
                NEW.uuid,
                NEW.contact_id,
                NEW.name,
                NEW.organization,
                NEW.street1,
                NEW.street2,
                NEW.street3,
                NEW.city,
                NEW.state_or_province,
                NEW.postal_code,
                NEW.country_code,
                NEW.telephone,
                NEW.email,
                NEW.log_entry_id,
                NEW.log_entry_delete_id,
                NEW.valid_from,
                NULL,
                new_ohid_from,
                NULL,
                NULL) RETURNING history_id INTO new_history_id;
                NEW.history_id = new_history_id;
            END IF;
        IF TG_OP = 'UPDATE' THEN
            UPDATE contact_representative_history
               SET valid_to = NEW.valid_from,
                   ohid_to = new_ohid_from,
                   next_history_id = new_history_id,
                   log_entry_delete_id = NEW.log_entry_delete_id
             WHERE history_id = OLD.history_id;
        END IF;
        IF TG_OP = 'UPDATE' AND NEW.log_entry_delete_id IS NOT NULL THEN
            RETURN NULL;
        ELSE
            RETURN NEW;
        END IF;
    ELSIF TG_OP = 'DELETE' THEN
        UPDATE contact_representative_history
           SET valid_to = CURRENT_TIMESTAMP,
               ohid_to = new_ohid_from
         WHERE history_id = OLD.history_id;
        RETURN OLD;
    END IF;
END;
$$
LANGUAGE plpgsql;

CREATE TRIGGER trigger_contact_representative
BEFORE INSERT OR UPDATE OR DELETE ON contact_representative
FOR EACH ROW EXECUTE PROCEDURE contact_representative_update();

COMMENT ON TRIGGER trigger_contact_representative ON contact_representative IS
'creates history of each contact_representative change: contact_representative -> contact_representative_history';
