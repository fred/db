CREATE TYPE notification.NOTIFIED_EVENT AS ENUM
(
    'created',
    'updated',
    'transferred',
    'deleted',
    'renewed'
);

CREATE TABLE notification.notification_queue
(
    change notification.NOTIFIED_EVENT NOT NULL,
    done_by_registrar INTEGER NOT NULL REFERENCES registrar(id),
    historyid_post_change INTEGER NOT NULL REFERENCES history(id),
    svtrid TEXT NOT NULL
);
