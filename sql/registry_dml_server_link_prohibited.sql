---
--- serverLinkProhibited state flag
---
WITH type AS
(
    SELECT ARRAY_AGG(id ORDER BY id) ids
      FROM enum_object_type
     WHERE name IN ('contact', 'keyset')
)
INSERT INTO enum_object_states (id, name, types, manual, external, importance)
     SELECT 34, 'serverLinkProhibited', type.ids, TRUE, TRUE, NULL
       FROM type;

INSERT INTO enum_object_states_desc (state_id, lang, description)
VALUES (34, 'CS', 'Není povoleno navázání objektu na objekty registru'),
       (34, 'EN', 'Object linking forbidden');
