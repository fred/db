-- system operational parameter
-- parameters are accessed through their id not name, so proper
-- numbering is essential

CREATE TABLE enum_parameters (
  id INTEGER CONSTRAINT enum_parameters_pkey PRIMARY KEY, -- primary identification 
  name VARCHAR(100) NOT NULL CONSTRAINT enum_parameters_name_key UNIQUE, -- descriptive name (informational)
  val VARCHAR(100) NOT NULL -- value of parameter
);

-- parameter 1 is for checking data model version and for applying upgrade
-- scripts
INSERT INTO enum_parameters (id, name, val) 
VALUES (1, 'model_version', '2.60.0');
-- parameter 2 is for updating table enum_tlds by data from url
-- http://data.iana.org/TLD/tlds-alpha-by-domain.txt
INSERT INTO enum_parameters (id, name, val) 
VALUES (2, 'tld_list_version', '2008013001');
-- parameter 9 is used to identify hour when objects are deleted
-- value is number of hours relative to date of operation
INSERT INTO enum_parameters (id, name, val) 
VALUES (9, 'regular_day_procedure_period', '0');
-- parameter 10 is used to identify time zone in which parameter 9 and 14
-- are specified
INSERT INTO enum_parameters (id, name, val) 
VALUES (10, 'regular_day_procedure_zone', 'Europe/Prague');
-- parameter 11 is used to change state of objects other than domain to
-- deleteCandidate. It is specified in granularity of months and means, period
-- during which object wasn't linked to other object and wasn't updated 
INSERT INTO enum_parameters (id, name, val) 
VALUES (11, 'object_registration_protection_period', '6');
-- parameter 12 is used to change protection period of deleted object handle
-- (contact, nsset, keyset). value is in months.
INSERT INTO enum_parameters (id, name, val)
VALUES (12, 'handle_registration_protection_period', '0');
-- parameter 13 is used as a suffix in object_registry roid string
-- this suffix should match pattern \w{1,8}
INSERT INTO enum_parameters (id, name, val)
VALUES (13, 'roid_suffix', 'EPP');
-- parameter 14 is used to identify hour when domains are moving outzone. 
-- value is number of hours relative to date of operation
INSERT INTO enum_parameters (id, name, val) 
VALUES (14, 'regular_day_outzone_procedure_period', '14');
-- parameter 19 is opportunity window in days before current ENUM domain validation expiration
-- for new ENUM domain validation to be appended after current ENUM domain validation
INSERT INTO enum_parameters (id, name, val)
VALUES (19, 'enum_validation_continuation_window', '14');


comment on table enum_parameters is
'Table of system operational parameters.
Meanings of parameters:

1 - model version - for checking data model version and for applying upgrade scripts
2 - tld list version - for updating table enum_tlds by data from url
9 - regular day procedure period - used to identify hour when objects are deleted and domains
    are moving outzone
10 - regular day procedure zone - used to identify time zone in which parameter 9 is specified
11 - change state of objects other than domain to deleteCandidate after specified period of time in months
     during which object was neither linked to other object nor updated
12 - protection period of deleted object handle (contact, nsset, keyset) in months
13 - a suffix in object_registry roid string, should match pattern \w{1,8}
14 - to identify hour when domains are moving outzone in number of hours relative to date of operation
19 - opportunity window in days before current ENUM domain validation expiration for new ENUM domain validation
     to be appended after current ENUM domain validation';
comment on column enum_parameters.id is 'primary identification';
comment on column enum_parameters.name is 'descriptive name of parameter - for information uses only';
comment on column enum_parameters.val is 'value of parameter';
