CREATE TYPE notification.RECIPIENT_GROUP AS ENUM
(
    'admin',
    'tech',
    'generic',
    'additional'
);

CREATE TYPE notification.MESSAGE_TYPE AS ENUM
(
    'email',
    'letter',
    'sms'
);

CREATE TABLE notification.object_state_changes
(
    object_state_id INTEGER NOT NULL CONSTRAINT notifcation_object_state_changes_email_object_state_id_fkey REFERENCES object_state (id),
    recipient_group notification.RECIPIENT_GROUP NOT NULL,
    message_type notification.MESSAGE_TYPE NOT NULL,
    CONSTRAINT object_state_changes_email_pkey PRIMARY KEY(object_state_id, recipient_group, message_type)
);

COMMENT ON TABLE notification.object_state_changes IS 'store information about successfull notification of object state changes';
COMMENT ON COLUMN notification.object_state_changes.object_state_id IS 'which object state change triggered notification';
COMMENT ON COLUMN notification.object_state_changes.recipient_group IS 'the group of recipients of the notification';
