CREATE TABLE notification.contact_data_reminder
(
    reminder_date DATE NOT NULL,
    contact_id INTEGER NOT NULL CONSTRAINT contact_data_reminder_contact_id_fkey REFERENCES object_registry(id),
    message_ident TEXT NOT NULL,
    CONSTRAINT contact_data_reminder_pkey PRIMARY KEY(reminder_date, contact_id)
);

COMMENT ON TABLE notification.contact_data_reminder IS 'store information about successfull notification of contact data reminder';
COMMENT ON COLUMN notification.contact_data_reminder.reminder_date IS 'date of notification';
COMMENT ON COLUMN notification.contact_data_reminder.contact_id IS 'data of which contact were subject of the notification';
COMMENT ON COLUMN notification.contact_data_reminder.message_ident IS 'ident of the notification message';
