CREATE TYPE notification.CONTACT_TYPE AS ENUM
(
    'email_address',
    'phone_number'
);

CREATE TABLE notification.object_state_additional_contact
(
    id BIGINT GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
    object_id INTEGER NOT NULL CONSTRAINT object_state_additional_contact_object_id_fkey REFERENCES object_registry(id),
    state_flag_id INTEGER NOT NULL CONSTRAINT object_state_additional_contact_state_flag_id_fkey REFERENCES enum_object_states(id),
    valid_from TIMESTAMP NOT NULL,
    type notification.CONTACT_TYPE NOT NULL,
    contacts VARCHAR[] NOT NULL,
    object_state_id INTEGER CONSTRAINT object_state_additional_contact_object_state_id_fkey REFERENCES object_state(id)
);

CREATE UNIQUE INDEX object_state_additional_conta_object_id_state_flag_id_valid_idx
                 ON notification.object_state_additional_contact (object_id, state_flag_id, valid_from, type);


CREATE OR REPLACE FUNCTION set_additional_contacts_notification()
RETURNS TRIGGER AS
$$
BEGIN
    IF (TG_OP <> 'INSERT') THEN
        RAISE EXCEPTION 'trigger only suitable for INSERT operation';
    END IF;

    IF NEW.recipient_group = 'additional'::notification.RECIPIENT_GROUP THEN

        WITH dlp AS
        (
            SELECT o.id,
                   o.valid_for_exdate_after,
                   COALESCE((SELECT i.valid_for_exdate_after
                               FROM domain_lifecycle_parameters i
                              WHERE o.id < i.id
                           ORDER BY i.id
                              LIMIT 1),
                            'infinity'::TIMESTAMP
                           ) AS valid_for_exdate_before,
                   o.outzone_unguarded_email_warning_period,
                   o.expiration_dns_protection_period,
                   o.expiration_letter_warning_period,
                   o.expiration_registration_protection_period
              FROM domain_lifecycle_parameters o
        ),
        d AS
        (
            SELECT domain.id,
                   domain.exdate,
                   (domain.exdate + dlp.outzone_unguarded_email_warning_period)::DATE AS outzone_unguarded_warning,
                   (domain.exdate + dlp.expiration_dns_protection_period)::DATE AS outzone,
                   (domain.exdate + dlp.expiration_letter_warning_period)::DATE AS delete_warning,
                   (domain.exdate + dlp.expiration_registration_protection_period)::DATE AS delete_candidate
              FROM domain
              JOIN dlp ON dlp.valid_for_exdate_after <= domain.exdate AND domain.exdate < dlp.valid_for_exdate_before
             WHERE domain.id = (SELECT object_id FROM object_state WHERE id = NEW.object_state_id)
        ),
        time_zone AS
        (
            SELECT val FROM enum_parameters WHERE name = 'regular_day_procedure_zone'
        ),
        s AS
        (
            SELECT os.state_id,
                   (os.valid_from AT TIME ZONE 'UTC' AT TIME ZONE time_zone.val)::DATE AS valid_from_local_date
              FROM object_state os
              JOIN d ON d.id = os.object_id,
                   time_zone
             WHERE os.id = NEW.object_state_id AND
                   os.valid_to IS NULL
        )
        UPDATE notification.object_state_additional_contact
           SET object_state_id = NEW.object_state_id
         WHERE id = ANY(SELECT osac.id
              FROM notification.object_state_additional_contact osac
              JOIN d ON d.id = osac.object_id AND
                        d.exdate = osac.valid_from::DATE
              JOIN s ON s.state_id = osac.state_flag_id
              JOIN enum_object_states eos ON eos.id = s.state_id
             WHERE ((eos.name = 'outzoneUnguardedWarning' AND
                     d.outzone_unguarded_warning <= s.valid_from_local_date AND s.valid_from_local_date < d.outzone)
                    OR
                    (eos.name = 'deleteWarning' AND
                     d.delete_warning <= s.valid_from_local_date AND s.valid_from_local_date < d.delete_candidate))
                   AND
                   ((osac.type = 'email_address'::notification.CONTACT_TYPE AND NEW.message_type = 'email'::notification.MESSAGE_TYPE) OR
                    (osac.type = 'phone_number'::notification.CONTACT_TYPE AND NEW.message_type = 'sms'::notification.MESSAGE_TYPE)));
    END IF;
    RETURN NEW;
END
$$ LANGUAGE plpgsql;

DROP TRIGGER IF EXISTS tr_set_additional_contacts_notification ON notification.object_state_changes;
CREATE TRIGGER tr_set_additional_contacts_notification
       BEFORE INSERT ON notification.object_state_changes
       FOR EACH ROW EXECUTE PROCEDURE set_additional_contacts_notification();
