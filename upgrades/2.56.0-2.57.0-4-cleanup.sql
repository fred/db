DROP FUNCTION IF EXISTS migrate_mail_archive_type_1;
DROP FUNCTION IF EXISTS migrate_mail_archive_type_2;
DROP FUNCTION IF EXISTS migrate_mail_archive_type_3;
DROP FUNCTION IF EXISTS migrate_mail_archive_type_4;
DROP FUNCTION IF EXISTS migrate_mail_archive_type_5;
DROP FUNCTION IF EXISTS migrate_mail_archive_type_6;
DROP FUNCTION IF EXISTS migrate_mail_archive_type_7;
DROP FUNCTION IF EXISTS migrate_mail_archive_type_8;
DROP FUNCTION IF EXISTS migrate_mail_archive_type_9;
DROP FUNCTION IF EXISTS migrate_mail_archive_type_10;
DROP FUNCTION IF EXISTS migrate_mail_archive_type_11;
DROP FUNCTION IF EXISTS migrate_mail_archive_type_12;
DROP FUNCTION IF EXISTS migrate_mail_archive_type_13;
DROP FUNCTION IF EXISTS migrate_mail_archive_type_14;
DROP FUNCTION IF EXISTS migrate_mail_archive_type_15;
DROP FUNCTION IF EXISTS migrate_mail_archive_type_16;
DROP FUNCTION IF EXISTS migrate_mail_archive_type_17;
DROP FUNCTION IF EXISTS migrate_mail_archive_type_18;
DROP FUNCTION IF EXISTS migrate_mail_archive_type_19;
DROP FUNCTION IF EXISTS migrate_mail_archive_type_20;
DROP FUNCTION IF EXISTS migrate_mail_archive_type_21;
DROP FUNCTION IF EXISTS migrate_mail_archive_type_22;
DROP FUNCTION IF EXISTS migrate_mail_archive_type_23;
DROP FUNCTION IF EXISTS migrate_mail_archive_type_24;
DROP FUNCTION IF EXISTS migrate_mail_archive_type_25;
DROP FUNCTION IF EXISTS migrate_mail_archive_type_26;
DROP FUNCTION IF EXISTS migrate_mail_archive_type_27;
DROP FUNCTION IF EXISTS migrate_mail_archive_type_28;
DROP FUNCTION IF EXISTS migrate_mail_archive_type_29;
DROP FUNCTION IF EXISTS migrate_mail_archive_type_30;
DROP FUNCTION IF EXISTS migrate_mail_archive_type_31;
DROP FUNCTION IF EXISTS migrate_mail_archive_type_32;
DROP FUNCTION IF EXISTS migrate_mail_archive_type_33;
DROP FUNCTION IF EXISTS migrate_mail_archive_type_34;
DROP FUNCTION IF EXISTS migrate_mail_archive_type_35;
DROP FUNCTION IF EXISTS migrate_mail_archive;
DROP FUNCTION IF EXISTS migrate_mail_archive_message_to_json;
DROP FUNCTION IF EXISTS migrate_mail_archive_response_to_json;
DROP FUNCTION IF EXISTS migrate_mail_archive_response_to_json_impl;
DROP FUNCTION IF EXISTS helper_add_old_new_pair;
DROP FUNCTION IF EXISTS helper_add_old_new_pair_disclose;
DROP FUNCTION IF EXISTS migrate_mail_header;


DELETE FROM enum_parameters
 WHERE name IN (
    'mojeid_async_sms_generation',
    'mojeid_async_letter_generation',
    'mojeid_async_email_generation');

COMMENT ON TABLE enum_parameters IS
'Table of system operational parameters.
Meanings of parameters:

1 - model version - for checking data model version and for applying upgrade scripts
2 - tld list version - for updating table enum_tlds by data from url
9 - regular day procedure period - used to identify hour when objects are deleted and domains
    are moving outzone
10 - regular day procedure zone - used to identify time zone in which parameter 9 is specified
11 - change state of objects other than domain to deleteCandidate after specified period of time in months
     during which object was neither linked to other object nor updated
12 - protection period of deleted object handle (contact, nsset, keyset) in months
13 - a suffix in object_registry roid string, should match pattern \w{1,8}
14 - to identify hour when domains are moving outzone in number of hours relative to date of operation
19 - opportunity window in days before current ENUM domain validation expiration for new ENUM domain validation
     to be appended after current ENUM domain validation';
