---
--- don't forget to update database schema version
---
UPDATE enum_parameters SET val = '2.55.1' WHERE id = 1;

UPDATE enum_object_states_desc
   SET description = 'VIP doména'
 WHERE state_id = 33 AND lang = 'CS';

UPDATE enum_object_states_desc
   SET description = 'VIP domain'
 WHERE state_id = 33 AND lang = 'EN';
