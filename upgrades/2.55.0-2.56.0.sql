---
--- don't forget to update database schema version
---
UPDATE enum_parameters SET val = '2.56.0' WHERE id = 1;

CREATE TABLE domain_auction (
        id BIGSERIAL CONSTRAINT domain_auction_pkey PRIMARY KEY,
        fqdn VARCHAR(255) NOT NULL CONSTRAINT domain_auction_fqdn_lower_check CHECK(LOWER(fqdn) = fqdn),
        created_at TIMESTAMP WITHOUT TIME ZONE NOT NULL DEFAULT CURRENT_TIMESTAMP,
        finished_at TIMESTAMP WITHOUT TIME ZONE DEFAULT NULL,
        released_at TIMESTAMP WITHOUT TIME ZONE DEFAULT NULL,
        external_id VARCHAR(255) CONSTRAINT domain_auction_external_id_key UNIQUE,
        next_event_after TIMESTAMP WITHOUT TIME ZONE DEFAULT NULL,
        winner_id INTEGER DEFAULT NULL CONSTRAINT domain_auction_winner_id_fkey REFERENCES object_registry(id),
        win_expires_at TIMESTAMP WITHOUT TIME ZONE DEFAULT NULL);

CREATE UNIQUE INDEX domain_auction_fqdn_unique ON domain_auction(fqdn) WHERE released_at IS NULL;

COMMENT ON TABLE domain_auction IS
'This table contains information about the progress of domain auction';
COMMENT ON COLUMN domain_auction.id IS 'unique automatically generated identifier';
COMMENT ON COLUMN domain_auction.fqdn IS 'auctioned domain fully qualified name';
COMMENT ON COLUMN domain_auction.created_at IS 'auction creation time';
COMMENT ON COLUMN domain_auction.finished_at IS 'auction end time';
COMMENT ON COLUMN domain_auction.released_at IS 'time of fqdn release for registration';
COMMENT ON COLUMN domain_auction.external_id IS 'external auction identifier';
COMMENT ON COLUMN domain_auction.next_event_after IS 'the earliest event can occur in this auction';
COMMENT ON COLUMN domain_auction.winner_id IS 'contact who is allowed to register a domain until win_expires_at';
COMMENT ON COLUMN domain_auction.win_expires_at IS 'the latest time by which the winner has to register the domain';

ALTER TABLE zone
      ADD COLUMN IF NOT EXISTS auction_enabled BOOLEAN NOT NULL DEFAULT FALSE;

COMMENT ON COLUMN zone.auction_enabled IS 'flag if domain lifecycle finishes by auction';

INSERT INTO enum_reason VALUES (68, 'Auction pending', 'Probíhá aukce doménového jména');
INSERT INTO enum_reason VALUES (69, 'Only the auction winner is authorized to register this domain', 'Pouze vítěz aukce je oprávněn registrovat tuto doménu');

SELECT setval('enum_reason_id_seq', 69);
