---
--- serverLinkProhibited state flag
---
WITH type AS
(
    SELECT ARRAY_AGG(type_id ORDER BY type_id) AS ids
      FROM (SELECT UNNEST(types)
              FROM enum_object_states
             WHERE id = 34
            UNION
            SELECT id
              FROM enum_object_type
             WHERE name = 'keyset') tmp(type_id)
)
UPDATE enum_object_states
   SET types = (SELECT ids FROM type)
 WHERE id = 34 AND
       name = 'serverLinkProhibited' AND
       manual AND
       external AND
       importance IS NULL
RETURNING id, name, types;

UPDATE enum_object_states_desc
   SET description = 'Není povoleno navázání objektu na objekty registru'
 WHERE state_id = 34 AND
       lang = 'CS';

UPDATE enum_object_states_desc
   SET description = 'Object linking forbidden'
 WHERE state_id = 34 AND
       lang = 'EN';

---
--- enum_reason
---
INSERT INTO enum_reason VALUES (70, 'Keyset linking forbidden', 'Není povoleno navázání keysetu na objekty registru');

SELECT setval('enum_reason_id_seq', 70);
