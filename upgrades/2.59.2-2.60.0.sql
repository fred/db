UPDATE enum_parameters SET val = '2.60.0' WHERE id = 1;

ALTER TABLE invoice ADD COLUMN filecsv INTEGER;
ALTER TABLE invoice ADD COLUMN file_csv_uuid UUID;

COMMENT ON COLUMN invoice.filecsv IS 'link to generated CSV file, it can be NULL till file is generated';
COMMENT ON COLUMN invoice.file_csv_uuid IS 'uuid of exported CSV file for external reference, it can be NULL if the file is not yet generated';

CREATE OR REPLACE FUNCTION invoice_update_file()
RETURNS trigger AS
$$
BEGIN
    IF TG_OP = 'INSERT' THEN
        IF NEW.file IS NOT NULL THEN
            RAISE EXCEPTION 'Inserting file (%) into invoice table. This is deprecated. Insert file_uuid instead.', NEW.file;
        END IF;
        IF NEW.filexml IS NOT NULL THEN
            RAISE EXCEPTION 'Inserting filexml (%) into invoice table. This is deprecated. Insert file_xml_uuid instead.', NEW.filexml;
        END IF;
        IF NEW.filecsv IS NOT NULL THEN
            RAISE EXCEPTION 'Inserting filecsv (%) into invoice table. This is deprecated. Insert file_csv_uuid instead.', NEW.filecsv;
        END IF;
    ELSIF TG_OP = 'UPDATE' THEN
        IF NEW.file <> OLD.file THEN
            RAISE EXCEPTION 'Updating file (%) in invoice table. This is deprecated. Update file_uuid instead.', NEW.file;
        END IF;
        IF NEW.filexml <> OLD.filexml THEN
            RAISE EXCEPTION 'Updating filexml (%) in invoice table. This is deprecated. Update file_xml_uuid instead.', NEW.filexml;
        END IF;
        IF NEW.filecsv <> OLD.filecsv THEN
            RAISE EXCEPTION 'Updating filecsv (%) in invoice table. This is deprecated. Update file_csv_uuid instead.', NEW.filecsv;
        END IF;
    END IF;
    IF NEW.file_uuid IS NULL THEN
        NEW.file := NULL;
    ELSE
        SELECT f.id
          FROM files f
         WHERE f.uuid = NEW.file_uuid
          INTO NEW.file;
    END IF;
    IF NEW.file_xml_uuid IS NULL THEN
        NEW.filexml := NULL;
    ELSE
        SELECT f.id
          FROM files f
         WHERE f.uuid = NEW.file_xml_uuid
          INTO NEW.filexml;
    END IF;
    IF NEW.file_csv_uuid IS NULL THEN
        NEW.filecsv := NULL;
    ELSE
        SELECT f.id
          FROM files f
         WHERE f.uuid = NEW.file_csv_uuid
          INTO NEW.filecsv;
    END IF;
    RETURN NEW;
END
$$ LANGUAGE plpgsql;

DROP TRIGGER IF EXISTS tr_invoice_update_file ON invoice;
CREATE TRIGGER tr_invoice_update_file
BEFORE INSERT OR UPDATE ON invoice
FOR EACH ROW EXECUTE PROCEDURE
invoice_update_file();
