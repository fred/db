---
--- don't forget to update database schema version
---
UPDATE enum_parameters SET val = '2.58.0' WHERE id = 1;

ALTER TABLE domain_blacklist
  RENAME COLUMN regexp TO fqdn;

ALTER TABLE domain_blacklist
  ADD COLUMN is_regex BOOLEAN DEFAULT TRUE,
  ADD COLUMN requested_by TEXT,
  ADD COLUMN uuid UUID NOT NULL UNIQUE DEFAULT gen_random_uuid();

ALTER TABLE domain_blacklist
  ALTER COLUMN is_regex DROP DEFAULT;

ALTER TABLE domain_blacklist
  ALTER COLUMN is_regex SET NOT NULL;

ALTER TABLE domain_blacklist
  ADD CONSTRAINT domain_blacklist_fqdn_lower_check CHECK(is_regex OR LOWER(fqdn) = fqdn);

ALTER TABLE domain_blacklist
  ALTER COLUMN valid_from SET DEFAULT CURRENT_TIMESTAMP;

CREATE INDEX domain_blacklist_fqdn_idx ON domain_blacklist (fqdn) WHERE NOT is_regex;

COMMENT ON COLUMN domain_blacklist.fqdn IS 'domain name or regular expression which is blocked';
COMMENT ON COLUMN domain_blacklist.is_regex IS 'set to false if value in fqdn is not regex but literal';
COMMENT ON COLUMN domain_blacklist.requested_by IS 'identificator of the service which set the record';
COMMENT ON COLUMN domain_blacklist.uuid IS 'uuid for external reference';

