CREATE SCHEMA IF NOT EXISTS notification AUTHORIZATION fred;

ALTER TABLE notification_queue
  SET SCHEMA notification;

ALTER TYPE notified_event
  SET SCHEMA notification;

CREATE TYPE notification.RECIPIENT_GROUP AS ENUM
(
    'admin',
    'tech',
    'generic',
    'additional'
);

CREATE TYPE notification.MESSAGE_TYPE AS ENUM
(
    'email',
    'letter',
    'sms'
);


CREATE TABLE notification.object_state_changes
(
    object_state_id INTEGER NOT NULL CONSTRAINT notifcation_object_state_changes_email_object_state_id_fkey REFERENCES object_state (id),
    recipient_group notification.RECIPIENT_GROUP NOT NULL,
    message_type notification.MESSAGE_TYPE NOT NULL,
    CONSTRAINT object_state_changes_email_pkey PRIMARY KEY(object_state_id, recipient_group, message_type)
);

COMMENT ON TABLE notification.object_state_changes IS 'store information about successfull notification of object state changes';
COMMENT ON COLUMN notification.object_state_changes.object_state_id IS 'which object state change triggered notification';
COMMENT ON COLUMN notification.object_state_changes.recipient_group IS 'the group of recipients of the notification';

INSERT INTO notification.object_state_changes (
SELECT ns.state_id,
       CASE nsm.emails
           WHEN 1 THEN 'admin'::notification.RECIPIENT_GROUP
           WHEN 2 THEN 'tech'::notification.RECIPIENT_GROUP
           WHEN 3 THEN 'generic'::notification.RECIPIENT_GROUP
           WHEN 4 THEN 'additional'::notification.RECIPIENT_GROUP
       END AS recipient_group,
       'email'::notification.MESSAGE_TYPE
  FROM notify_statechange ns
  LEFT JOIN notify_statechange_map nsm ON ns.type = nsm.id
);


CREATE TABLE notification.contact_data_reminder
(
    reminder_date DATE NOT NULL,
    contact_id INTEGER NOT NULL CONSTRAINT contact_data_reminder_contact_id_fkey REFERENCES object_registry(id),
    message_ident TEXT NOT NULL,
    CONSTRAINT contact_data_reminder_pkey PRIMARY KEY(reminder_date, contact_id)
);

COMMENT ON TABLE notification.contact_data_reminder IS 'store information about successfull notification of contact data reminder';
COMMENT ON COLUMN notification.contact_data_reminder.reminder_date IS 'date of notification';
COMMENT ON COLUMN notification.contact_data_reminder.contact_id IS 'data of which contact were subject of the notification';
COMMENT ON COLUMN notification.contact_data_reminder.message_ident IS 'ident of the notification message';

INSERT INTO notification.contact_data_reminder (reminder_date, contact_id, message_ident)
SELECT reminder_date, contact_id, message_id::TEXT
  FROM reminder_contact_message_map rcmm;



CREATE OR REPLACE FUNCTION set_additional_contacts_notification()
RETURNS TRIGGER AS
$$
BEGIN
    IF (TG_OP <> 'INSERT') THEN
        RAISE EXCEPTION 'trigger only suitable for INSERT operation';
    END IF;

    IF NEW.recipient_group = 'additional'::notification.RECIPIENT_GROUP THEN

        WITH dlp AS
        (
            SELECT o.id,
                   o.valid_for_exdate_after,
                   COALESCE((SELECT i.valid_for_exdate_after
                               FROM domain_lifecycle_parameters i
                              WHERE o.id < i.id
                           ORDER BY i.id
                              LIMIT 1),
                            'infinity'::TIMESTAMP
                           ) AS valid_for_exdate_before,
                   o.outzone_unguarded_email_warning_period,
                   o.expiration_dns_protection_period,
                   o.expiration_letter_warning_period,
                   o.expiration_registration_protection_period
              FROM domain_lifecycle_parameters o
        ),
        d AS
        (
            SELECT domain.id,
                   domain.exdate,
                   (domain.exdate + dlp.outzone_unguarded_email_warning_period)::DATE AS outzone_unguarded_warning,
                   (domain.exdate + dlp.expiration_dns_protection_period)::DATE AS outzone,
                   (domain.exdate + dlp.expiration_letter_warning_period)::DATE AS delete_warning,
                   (domain.exdate + dlp.expiration_registration_protection_period)::DATE AS delete_candidate
              FROM domain
              JOIN dlp ON dlp.valid_for_exdate_after <= domain.exdate AND domain.exdate < dlp.valid_for_exdate_before
             WHERE domain.id = (SELECT object_id FROM object_state WHERE id = NEW.object_state_id)
        ),
        time_zone AS
        (
            SELECT val FROM enum_parameters WHERE name = 'regular_day_procedure_zone'
        ),
        s AS
        (
            SELECT os.state_id,
                   (os.valid_from AT TIME ZONE 'UTC' AT TIME ZONE time_zone.val)::DATE AS valid_from_local_date
              FROM object_state os
              JOIN d ON d.id = os.object_id,
                   time_zone
             WHERE os.id = NEW.object_state_id AND
                   os.valid_to IS NULL
        )
        UPDATE notification.object_state_additional_contact
           SET object_state_id = NEW.object_state_id
         WHERE id = ANY(SELECT osac.id
              FROM notification.object_state_additional_contact osac
              JOIN d ON d.id = osac.object_id AND
                        d.exdate = osac.valid_from::DATE
              JOIN s ON s.state_id = osac.state_flag_id
              JOIN enum_object_states eos ON eos.id = s.state_id
             WHERE ((eos.name = 'outzoneUnguardedWarning' AND
                     d.outzone_unguarded_warning <= s.valid_from_local_date AND s.valid_from_local_date < d.outzone)
                    OR
                    (eos.name = 'deleteWarning' AND
                     d.delete_warning <= s.valid_from_local_date AND s.valid_from_local_date < d.delete_candidate))
                   AND
                   ((osac.type = 'email_address'::notification.CONTACT_TYPE AND NEW.message_type = 'email'::notification.MESSAGE_TYPE) OR
                    (osac.type = 'phone_number'::notification.CONTACT_TYPE AND NEW.message_type = 'sms'::notification.MESSAGE_TYPE)));
    END IF;
    RETURN NEW;
END
$$ LANGUAGE plpgsql;


DROP TRIGGER IF EXISTS tr_set_additional_contacts_notification ON notification.object_state_changes;
CREATE TRIGGER tr_set_additional_contacts_notification
       BEFORE INSERT ON notification.object_state_changes
       FOR EACH ROW EXECUTE PROCEDURE set_additional_contacts_notification();

INSERT INTO notification.object_state_additional_contact
       (object_id,
        state_flag_id,
        valid_from,
        type,
        contacts,
        object_state_id)
SELECT noudae.domain_id,
       eos.id,
       dh.exdate::TIMESTAMP,
       'email_address'::notification.CONTACT_TYPE,
       ARRAY_AGG(DISTINCT LOWER(noudae.email)),
       MAX(noudae.state_id)
  FROM enum_object_states eos,
       notify_outzone_unguarded_domain_additional_email noudae
  JOIN domain_history dh ON dh.id = noudae.domain_id
  JOIN history h ON h.id = dh.historyid
 WHERE eos.name = 'outzoneUnguardedWarning' AND
       h.valid_from <= noudae.crdate AND (h.valid_to IS NULL OR noudae.crdate < h.valid_to)
 GROUP BY noudae.domain_id, dh.exdate, eos.id;

INSERT INTO domain_lifecycle_parameters (
    valid_for_exdate_after,
    expiration_notify_period,
    outzone_unguarded_email_warning_period,
    expiration_dns_protection_period,
    expiration_letter_warning_period,
    expiration_registration_protection_period,
    validation_notify1_period,
    validation_notify2_period)
VALUES (NOW()::DATE - '33 DAYS'::INTERVAL, '-30DAYS', '25DAYS', '30DAYS', '56DAYS', '61DAYS', '-30DAYS', '-15DAYS');
