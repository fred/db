---
--- Issue #16 - extend range of registrar_certification.classification attribute from 0-5 to 0-100
---
UPDATE registrar_certification
   SET classification = 50 + 10 * classification;
