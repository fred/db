#!/bin/bash
./add_uuid_column.py $@ --table-name files --column-name uuid
#!/bin/bash
./add_uuid_column.py $@ --table-name mail_archive --column-name uuid
#!/bin/bash
./add_uuid_column.py $@ --table-name message_archive --column-name uuid
