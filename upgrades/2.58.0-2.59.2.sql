---
--- don't forget to update database schema version
---
UPDATE enum_parameters SET val = '2.59.2' WHERE id = 1;

ALTER TABLE domain_auction
        ADD uuid UUID;
UPDATE domain_auction SET uuid = external_id::UUID;
ALTER TABLE domain_auction
      ALTER uuid SET NOT NULL,
      ALTER uuid SET DEFAULT gen_random_uuid(),
        ADD CONSTRAINT domain_auction_uuid_unique UNIQUE (uuid);
COMMENT ON COLUMN domain_auction.id IS 'unique automatically generated numerical identifier';
COMMENT ON COLUMN domain_auction.uuid IS 'unique automatically generated uuid identifier';

CREATE INDEX CONCURRENTLY IF NOT EXISTS domain_keyset_idx ON domain(keyset);
CREATE INDEX CONCURRENTLY IF NOT EXISTS domain_history_keyset_idx ON domain_history(keyset);
