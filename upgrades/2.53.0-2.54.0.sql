---
--- contact states
---
INSERT INTO enum_object_states (id, name, types, manual, external, importance)
VALUES (34, 'serverLinkProhibited', '{1}', 't', 't', NULL);

INSERT INTO enum_object_states_desc (state_id, lang, description)
VALUES (34, 'CS', 'Není povoleno navázání kontaktu na objekty registru'),
       (34, 'EN', 'Contact linking forbidden');

---
--- enum_reason
---
INSERT INTO enum_reason VALUES (67, 'Contact linking forbidden', 'Není povoleno navázání kontaktu na objekty registru');

SELECT setval('enum_reason_id_seq', 67);
