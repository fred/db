---
--- don't forget to update database schema version
---
UPDATE enum_parameters SET val = '2.57.0' WHERE id = 1;

CREATE TABLE mail_template_messenger_migration
(
    mail_type_id INTEGER,
    mail_type TEXT NOT NULL,
    version INTEGER NOT NULL,
    subject TEXT NOT NULL CHECK (REPLACE(subject, ' ', '') = subject AND LENGTH(subject) > 0),
    subject_uuid UUID NOT NULL,
    body TEXT NOT NULL CHECK (REPLACE(body, ' ', '') = body AND LENGTH(body) > 0),
    body_uuid UUID NOT NULL
);

\COPY mail_template_messenger_migration (mail_type, version, subject, subject_uuid, body, body_uuid) FROM 'mail_template_messenger_migration_data.csv' WITH CSV DELIMITER ',' HEADER

UPDATE mail_template_messenger_migration mig
   SET mail_type_id = (SELECT id FROM mail_type WHERE name = mig.mail_type);

DO LANGUAGE PLPGSQL
$$
DECLARE
    template_name constant text := 'sendauthinfo_epp';
BEGIN
    IF NOT EXISTS (SELECT
                     FROM mail_template mtt
                     JOIN mail_type mt ON mt.id = mtt.mail_type_id
                    WHERE mt.name = template_name AND version = 2)
    THEN
        DELETE
          FROM mail_template_messenger_migration mm
         USING mail_type mt
         WHERE mm.mail_type_id = mt.id AND mt.name = template_name AND mm.version = 1;

        UPDATE mail_template_messenger_migration mm
           SET version = 1
          FROM mail_type mt
         WHERE mm.mail_type_id = mt.id AND mt.name = template_name AND mm.version = 2;
    END IF;
END
$$;

ALTER TABLE mail_template_messenger_migration
       DROP COLUMN mail_type;

ALTER TABLE mail_template_messenger_migration
      ALTER COLUMN mail_type_id SET NOT NULL;

ALTER TABLE mail_template_messenger_migration
        ADD CONSTRAINT mail_template_messenger_migration_pkey PRIMARY KEY (mail_type_id, version);

ALTER TABLE mail_template_messenger_migration
        ADD CONSTRAINT mail_template_messenger_migration_mail_type_id_version_fkey
            FOREIGN KEY (mail_type_id, version)
            REFERENCES mail_template(mail_type_id, version)
            DEFERRABLE INITIALLY DEFERRED;

CREATE INDEX CONCURRENTLY mail_archive_moddate_idx ON mail_archive USING btree (moddate);
CREATE INDEX CONCURRENTLY message_archive_moddate_idx ON message_archive USING btree (moddate);
CREATE INDEX CONCURRENTLY reminder_contact_message_map_message_id_idx
    ON reminder_contact_message_map USING btree (message_id);


CREATE INDEX CONCURRENTLY public_request_answer_email_id_idx
    ON public_request USING btree (answer_email_id);

CREATE INDEX CONCURRENTLY public_request_messages_map_mail_archive_id_idx
    ON public_request_messages_map USING btree (mail_archive_id);

CREATE INDEX CONCURRENTLY notify_statechange_mail_id_idx
    ON notify_statechange USING btree (mail_id);

CREATE INDEX CONCURRENTLY invoice_mails_mailid_idx
    ON invoice_mails USING btree (mailid);
